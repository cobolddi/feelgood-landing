<?php @include('template-parts/header.php') ?>

<section class="BackPainSection">
	<div class="BgCenterContent">
		<div class="CenterContent">
			<div class="container">
				<h2>Do You suffer from<br><span>Any Of the following back pain</span></h2>
			</div>
		</div>
	</div>
	<div class="BackPainCards">
		<div class="container">
			<div class="row">
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="assets/img/back.png" alt=""></span>
						<h4>Back Pain</h4>
						<div class="ContentBox">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, officia et obcaecati tempore atque consequuntur totam odio aliquid adipisci ullam consectetur veritatis error maxime nostrum. Illo tenetur facilis vero deserunt!</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="assets/img/snor.png" alt=""></span>
						<h4>Snoring & <br>Breathing Difficulty</h4>
						<div class="ContentBox">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, officia et obcaecati tempore atque consequuntur totam odio aliquid adipisci ullam consectetur veritatis error maxime nostrum. Illo tenetur facilis vero deserunt!</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="assets/img/arthiritis.png" alt=""></span>
						<h4>Arthiritis</h4>
						<div class="ContentBox">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, officia et obcaecati tempore atque consequuntur totam odio aliquid adipisci ullam consectetur veritatis error maxime nostrum. Illo tenetur facilis vero deserunt!</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="assets/img/swell.png" alt=""></span>
						<h4>Swelling & Circulation</h4>
						<div class="ContentBox">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, officia et obcaecati tempore atque consequuntur totam odio aliquid adipisci ullam consectetur veritatis error maxime nostrum. Illo tenetur facilis vero deserunt!</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="assets/img/heartburn.png" alt=""></span>
						<h4>Heartburn & <br>Acid Reflux</h4>
						<div class="ContentBox">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, officia et obcaecati tempore atque consequuntur totam odio aliquid adipisci ullam consectetur veritatis error maxime nostrum. Illo tenetur facilis vero deserunt!</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="assets/img/chronic.png" alt=""></span>
						<h4>Chronic Pain</h4>
						<div class="ContentBox">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, officia et obcaecati tempore atque consequuntur totam odio aliquid adipisci ullam consectetur veritatis error maxime nostrum. Illo tenetur facilis vero deserunt!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Section AdjustableBedButton">
		<div class="container">
			<a href="#" class="ToggleButton">Find Out How An Adjustable Bed Can Help</a>
		</div>
	</div>
</section>

<section>
	<div class="Section HeadingBlock">
		<div class="container">
			<h2>Here is the Solution for you</h2>
		</div>
	</div>
	<div class="Section SliderBlock">
		<div class="container">
			<h2>Find the perfect sleeping position</h2>
			<div class="slider">
			  	<div class="BenefitsSlide">
			  		<h3>Zero-g Position Benefits</h3>
			  		<div class="WhiteBg">
			  			<div class="row">
				  			<div class="col-12 col-md-5">
				  				<img src="assets/img/zero.jpg" alt="">
				  			</div>
				  			<div class="col-12 col-md-7">
				  				<div class="ContentBlock">
				  					<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex corrupti recusandae voluptas sed laboriosam expedita laudantium velit accusantium nihil calculation!</h4>
				  					<ul>
				  						<li>
				  							<span>01/</span><br>
				  							Less Back Pain and Neck pain
				  						</li>
				  						<li>
				  							<span>02/</span><br>
				  							Reduce <br>Snoring
				  						</li>
				  						<li>
				  							<span>03/</span><br>
				  							Better Heart Health
				  						</li>
				  						<li>
				  							<span>04/</span><br>
				  							Ease Heartburn & Acid Reflux
				  						</li>
				  						<li>
				  							<span>05/</span><br>
				  							Swelling Reduction
				  						</li>
				  						<li>
				  							<span>06/</span><br>
				  							Better Breathing
				  						</li>
				  					</ul>
				  				</div>
				  			</div>
				  		</div>
			  		</div>
			  	</div>
			  	<div class="BenefitsSlide">
			  		<h3>Feet Up Position</h3>
			  		<div class="WhiteBg">
			  			<div class="row">
				  			<div class="col-12 col-md-5">
				  				<img src="assets/img/zero.jpg" alt="">
				  			</div>
				  			<div class="col-12 col-md-7">
				  				<div class="ContentBlock">
				  					<h4>Raising the legs above heart level will repairs itself from a day of running around or sitting and let the body receive better circulation. </h4>
				  					<ul>
				  						<li>
				  							<span>01/</span><br>
				  							Better Blood Circulation
				  						</li>
				  						<li>
				  							<span>02/</span><br>
				  							Minimize Chronic Back pain And Sciatica Symptoms
				  						</li>
				  						<li>
				  							<span>03/</span><br>
				  							Reduce Edema in the legs and Feet
				  						</li>
				  						<li>
				  							<span>04/</span><br>
				  							Lessen Risk pf Swollen feet and Tired Leg pain
				  						</li>
				  						<li>
				  							<span>05/</span><br>
				  							Alleviate Symptoms Of Varicous Veins And Lymphedema
				  						</li>
				  						<li>
				  							<span>06/</span><br>
				  							Prevent DVT
				  						</li>
				  					</ul>
				  				</div>
				  			</div>
				  		</div>
			  		</div>
			  	</div>
			  	<div class="BenefitsSlide">
			  		<h3>Head Up Position</h3>
			  		<div class="WhiteBg">
			  			<div class="row">
				  			<div class="col-12 col-md-5">
				  				<img src="assets/img/zero.jpg" alt="">
				  			</div>
				  			<div class="col-12 col-md-7">
				  				<div class="ContentBlock">
				  					<h4>Elevated Position in an adjustable bed opens the breathing passages to improve sleep problems due to health issues and snoring.</h4>
				  					<ul>
				  						<li>
				  							<span>01/</span><br>
				  							Stop Mouth Breathing
				  						</li>
				  						<li>
				  							<span>02/</span><br>
				  							End Post-Nasal Drop
				  						</li>
				  						<li>
				  							<span>03/</span><br>
				  							Curb Snoring and Sleep Apnea
				  						</li>
				  						<li>
				  							<span>04/</span><br>
				  							Alleviate Heart Congestion and Inflammation
				  						</li>
				  						<li>
				  							<span>05/</span><br>
				  							Lessen Shortness Of Breath
				  						</li>
				  						<li>
				  							<span>06/</span><br>
				  							COPD Relief
				  						</li>
				  					</ul>
				  				</div>
				  			</div>
				  		</div>
			  		</div>
			  	</div>
			</div>
		</div>
	</div>
</section>

<section class="ProductCategoryWithHead">
	<div class="Section HeadingBlock">
		<div class="container">
			<h2>Product Category</h2>
		</div>
	</div>

	
		<div class="tabs ProductCategoryCards">
			<div class="container">
			  	<ul class="tabs-list ">
				    <li>
				     	<div class="ProductCards">
							<h4>Contour Elite</h4>
							<img src="assets/img/contour.png" alt="">
							<a href="#tab1" class="ProductLink">Know More</a>
						</div>
				    </li>
				    <li >
						<div class="ProductCards">
							<h4>Pure Slim</h4>
							<img src="assets/img/Pureslim-small.jpg" alt="">
							<a href="#tab2" class="ProductLink">Know More</a>
						</div>
				    </li>
				    <li >
						<div class="ProductCards">
							<h4>Essence</h4>
							<img src="assets/img/essence-small.jpg" alt="">
							<a href="#tab3" class="ProductLink">Know More</a>
						</div>
				    </li>
			  	</ul>
			</div>


			<div id="tab1" class="tab">
				<div class="ProductFullContent">
					<div class="InsideProduct">
						<div class="container">
							<h2>Contour Elite</h2>
							<img src="assets/img/contour-elite.png" alt="">
						</div>
					</div>
					<div class="featuresIcon">
						<div class="container">
							<ul>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-wireless"></use>
										</svg>
									</span>
									<p>Wireless Remote</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-headfoot"></use>
										</svg>
									</span>
									<p>Head & Foot Controls</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-flatposition"></use>
										</svg>
									</span>
									<p>One Touch Flat Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-zeroclearance"></use>
										</svg>
									</span>
									<p>Zero Clearance</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-zeroG"></use>
										</svg>
									</span>
									<p>Zero G</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-programmable"></use>
										</svg>
									</span>
									<p>2 Programmable Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-wallsaver"></use>
										</svg>
									</span>
									<p>Wall Saver</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-massage"></use>
										</svg>
									</span>
									<p>Massage</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-autohead"></use>
										</svg>
									</span>
									<p>Auto Head Tilt</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-dualusb"></use>
										</svg>
									</span>
									<p>Dual USB Ports</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-extensiondeck"></use>
										</svg>
									</span>
									<p>Extension Deck</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-zerostandby"></use>
										</svg>
									</span>
									<p>Zero Standby Power System</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-poweroutage"></use>
										</svg>
									</span>
									<p>Power Outage Protection</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-silentdrive"></use>
										</svg>
									</span>
									<p>Silent Drive Motors</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-underbed"></use>
										</svg>
									</span>
									<p>Under Bed Lighting</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-legoption"></use>
										</svg>
									</span>
									<p>Interchangeable Leg Options</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-bluetooth"></use>
										</svg>
									</span>
									<p>Bluetooth Capability</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="tab2" class="tab">
			    <div class="ProductFullContent">
					<div class="InsideProduct">
						<div class="container">
							<h2>Pure Slim</h2>
							<img src="assets/img/pure-slim.png" alt="">
						</div>
					</div>
					<div class="featuresIcon">
						<div class="container">
							<ul>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-wireless"></use>
										</svg>
									</span>
									<p>Wireless Remote</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-headfoot"></use>
										</svg>
									</span>
									<p>Head & Foot Controls</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-flatposition"></use>
										</svg>
									</span>
									<p>One Touch Flat Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-zeroclearance"></use>
										</svg>
									</span>
									<p>Zero Clearance</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-zeroG"></use>
										</svg>
									</span>
									<p>Zero G</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-programmable"></use>
										</svg>
									</span>
									<p>2 Programmable Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-blackmetal"></use>
										</svg>
									</span>
									<p>Black Metal 6+3 Legs</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-headboard"></use>
										</svg>
									</span>
									<p>Headboard Brackets</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-outage"></use>
										</svg>
									</span>
									<p>Power Outage Protection</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-silentdrive"></use>
										</svg>
									</span>
									<p>Silent Drive Motors</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-standby"></use>
										</svg>
									</span>
									<p>Zero Standby Power System</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-bluetooth"></use>
										</svg>
									</span>
									<p>Bluetooth Capability</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="tab3" class="tab">
			    <div class="ProductFullContent">
					<div class="InsideProduct">
						<div class="container">
							<h2>Essence</h2>
							<img src="assets/img/essence.png" alt="">
						</div>
					</div>
					<div class="featuresIcon">
						<div class="container">
							<ul>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-wireless"></use>
										</svg>
									</span>
									<p>Wireless Remote</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-headfoot"></use>
										</svg>
									</span>
									<p>Head & Foot Controls</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-flatposition"></use>
										</svg>
									</span>
									<p>One Touch Flat Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-zeroclearance"></use>
										</svg>
									</span>
									<p>Zero Clearance</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-zeroG"></use>
										</svg>
									</span>
									<p>Zero G</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-programmable"></use>
										</svg>
									</span>
									<p>2 Programmable Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-wallsaver"></use>
										</svg>
									</span>
									<p>Wall Saver</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-massage"></use>
										</svg>
									</span>
									<p>Massage</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-autohead"></use>
										</svg>
									</span>
									<p>Auto Head Tilt</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-dualusb"></use>
										</svg>
									</span>
									<p>Dual USB Ports</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-extensiondeck"></use>
										</svg>
									</span>
									<p>Extension Deck</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-zerostandby"></use>
										</svg>
									</span>
									<p>Zero Standby Power System</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-poweroutage"></use>
										</svg>
									</span>
									<p>Power Outage Protection</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-silentdrive"></use>
										</svg>
									</span>
									<p>Silent Drive Motors</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-underbed"></use>
										</svg>
									</span>
									<p>Under Bed Lighting</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-legoption"></use>
										</svg>
									</span>
									<p>Interchangeable Leg Options</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="assets/img/product.svg#icon-bluetooth"></use>
										</svg>
									</span>
									<p>Bluetooth Capability</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>

	
	
	
</section>



<?php @include('template-parts/footer.php') ?>
