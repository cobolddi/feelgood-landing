$(document).ready(function() {

	$('.slider').slick({
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,
        responsive: [{
          breakpoint: 600,
	          settings: {
	            slidesToShow: 1,
	            slidesToScroll: 1
	          }
	        },
	        {
	           breakpoint: 400,
	           settings: {
	              arrows: false,
	              slidesToShow: 1,
	              slidesToScroll: 1
	           }
	        }]
	    });
	
	$(".ToggleButton").click(function(e){
		e.preventDefault();
		// $('.ContentBox').toggleClass('collapsible');
		$('.ContentBox').toggleClass('collapsible');
		$('.ContentBox p').fadeToggle();
		var collapseClass = $('.ContentBox').hasClass('collapsible');
		console.log('collapseClass', collapseClass);
		
		
		if(collapseClass){
			$('.ToggleButton').html("View Less");
		} else{
			$('.ToggleButton').html("Find Out How An Adjustable Bed Can Help");
		}
	});

	$(".tabs-list li a").click(function(e){
	     e.preventDefault();
	});

	$(".tabs-list li").click(function(){
		var tabid = $(this).find("a").attr("href");
		$(".tabs-list li,.tabs div.tab").removeClass("active");   // removing active class from tab

		$(".tab").hide();   // hiding open tab
		$(tabid).show();    // show tab
		$(this).addClass("active"); //  adding active class to clicked tab
	});



});
