<?php 
/*
Template Name: FeelgoodLandingPage
*/
// get_header()
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>feel good</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/logo.png">
	
    <link href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/css/vendor.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/css/styles.min.css" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body>

	<header class="Header">
		<div class="LogoSection">
			<div class="container">
				<a href="https://feelgoodsleepsystems.com/"><img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/logo.png" alt=""></a>
			</div>
		</div>

		<div class="BannerSection" style="background: url(<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/banner.jpg) no-repeat;">
			<div class="BannerContent">
				<div class="container">
					<h1 class="TopBar">Is It Time To <br><span>Go Adjustable?</span></h1>
				</div>
			</div>
		</div>
	</header>

	<main>

<section class="BackPainSection">
	<div class="BgCenterContent">
		<div class="CenterContent">
			<div class="container">
				<h2>Do You suffer from<br><span>Any Of the following back pain</span></h2>
			</div>
		</div>
	</div>
	<div class="BackPainCards">
		<div class="container">
			<div class="row">
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/back.png" alt=""></span>
						<h4>Back Pain</h4>
						<div class="ContentBox">
							<p>A traditional bed does not accommodate the natural contours of your body, resulting in increased pressure on your spine. Research has shown elevating your legs and upper body can help relieve the pressure put on your back while you sleep.</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/arthiritis.png" alt=""></span>
						<h4>Arthiritis</h4>
						<div class="ContentBox">
							<p>Arthritis leaves your joints tender and inflamed. Our unique Zero Gravity position will help relieve pressure on your joints and when coupled with our therapeutic massage system can help relieve the aches of sore joints.!</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/chronic.png" alt=""></span>
						<h4>Chronic Pain</h4>
						<div class="ContentBox">
							<p>A traditional bad does not accommodate the natural contours of your body, resulting in increased pressure on your spin. Research has shown elevating your legs and upper body can help relieve the pressure put on your back while you sleep.</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/swell.png" alt=""></span>
						<h4>Swelling & Circulation</h4>
						<div class="ContentBox">
							<p>Conditions such as oedema cause swelling due to fluids pooling in your limbs. By elevating the effected limb above your heart pressure is released and circulation improved. Our adjustable therapeutic massage system can also help alleviate the pain associated with swelling.</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/heartburn.png" alt=""></span>
						<h4>Heartburn & <br>Acid Reflux</h4>
						<div class="ContentBox">
							<p>Doctors recommend keeping your upper body elevated to prevent acid reflux. If you regularly lose sleep to heartburn or reflux an electric bed will greatly improve your quality of sleep. Research has also shown that an elevated upper body can help encourage healthy digestion.</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="CardWithContent">
						<span><img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/snor.png" alt=""></span>
						<h4>Snoring & <br>Breathing Difficulty</h4>
						<div class="ContentBox">
							<p>Elevating your upper body greatly reduces the pressure on your chest and respiratory system. By doing this you are allowing oxygen to flow easily through your airways and lungs therefore reducing your snoring and other respiratory conditions.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Section AdjustableBedButton">
		<div class="container">
			<a href="#" class="ToggleButton">Find Out How An Adjustable Bed Can Help</a>
		</div>
	</div>
</section>

<section>
	<div class="Section HeadingBlock">
		<div class="container">
			<h2>Here is the Solution for you</h2>
		</div>
	</div>
	<div class="Section SliderBlock">
		<div class="container">
			<h2>Find the perfect sleeping position</h2>
			<div class="slider">
			  	<div class="BenefitsSlide">
			  		<h3>Zero-g Position Benefits</h3>
			  		<div class="WhiteBg">
			  			<div class="row">
				  			<div class="col-12 col-md-5">
				  				<img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/zero.jpg" alt="">
				  			</div>
				  			<div class="col-12 col-md-7">
				  				<div class="ContentBlock">
				  					<h4>In this position the angle of the thighs and torso are aligned as the upper body and head are raised a bit, and the knees slightly bent as the legs are raised to about heart level to reduce  back pressure and increase circulation.</h4>
				  					<ul>
				  						<li>
				  							<span>01/</span><br>
				  							Less Back Pain and Neck pain
				  						</li>
				  						<li>
				  							<span>02/</span><br>
				  							Reduce <br>Snoring
				  						</li>
				  						<li>
				  							<span>03/</span><br>
				  							Better Heart Health
				  						</li>
				  						<li>
				  							<span>04/</span><br>
				  							Ease Heartburn & Acid Reflux
				  						</li>
				  						<li>
				  							<span>05/</span><br>
				  							Swelling Reduction
				  						</li>
				  						<li>
				  							<span>06/</span><br>
				  							Better Breathing
				  						</li>
				  					</ul>
				  				</div>
				  			</div>
				  		</div>
			  		</div>
			  	</div>
			  	<div class="BenefitsSlide">
			  		<h3>Feet Up Position</h3>
			  		<div class="WhiteBg">
			  			<div class="row">
				  			<div class="col-12 col-md-5">
				  				<img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/zero.jpg" alt="">
				  			</div>
				  			<div class="col-12 col-md-7">
				  				<div class="ContentBlock">
				  					<h4>Raising the legs above heart level will repairs itself from a day of running around or sitting and let the body receive better circulation. </h4>
				  					<ul>
				  						<li>
				  							<span>01/</span><br>
				  							Better Blood Circulation
				  						</li>
				  						<li>
				  							<span>02/</span><br>
				  							Minimize Chronic Back pain And Sciatica Symptoms
				  						</li>
				  						<li>
				  							<span>03/</span><br>
				  							Reduce Edema in the legs and Feet
				  						</li>
				  						<li>
				  							<span>04/</span><br>
				  							Lessen Risk pf Swollen feet and Tired Leg pain
				  						</li>
				  						<li>
				  							<span>05/</span><br>
				  							Alleviate Symptoms Of Varicous Veins And Lymphedema
				  						</li>
				  						<li>
				  							<span>06/</span><br>
				  							Prevent DVT
				  						</li>
				  					</ul>
				  				</div>
				  			</div>
				  		</div>
			  		</div>
			  	</div>
			  	<div class="BenefitsSlide">
			  		<h3>Head Up Position</h3>
			  		<div class="WhiteBg">
			  			<div class="row">
				  			<div class="col-12 col-md-5">
				  				<img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/zero.jpg" alt="">
				  			</div>
				  			<div class="col-12 col-md-7">
				  				<div class="ContentBlock">
				  					<h4>Elevated Position in an adjustable bed opens the breathing passages to improve sleep problems due to health issues and snoring.</h4>
				  					<ul>
				  						<li>
				  							<span>01/</span><br>
				  							Stop Mouth Breathing
				  						</li>
				  						<li>
				  							<span>02/</span><br>
				  							End Post-Nasal Drop
				  						</li>
				  						<li>
				  							<span>03/</span><br>
				  							Curb Snoring and Sleep Apnea
				  						</li>
				  						<li>
				  							<span>04/</span><br>
				  							Alleviate Heart Congestion and Inflammation
				  						</li>
				  						<li>
				  							<span>05/</span><br>
				  							Lessen Shortness Of Breath
				  						</li>
				  						<li>
				  							<span>06/</span><br>
				  							COPD Relief
				  						</li>
				  					</ul>
				  				</div>
				  			</div>
				  		</div>
			  		</div>
			  	</div>
			</div>
		</div>
	</div>
</section>

<section class="ProductCategoryWithHead">
	<div class="Section HeadingBlock">
		<div class="container">
			<h2>Product Category</h2>
		</div>
	</div>

	
		<div class="tabs ProductCategoryCards">
			<div class="container">
			  	<ul class="tabs-list ">
				    <li>
				     	<div class="ProductCards">
							<h4>Contour Elite</h4>
							<img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/contour.png" alt="">
							<a href="#tab1" class="ProductLink">Know More</a>
						</div>
				    </li>
				    <li >
						<div class="ProductCards">
							<h4>Pure Slim</h4>
							<img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/Pureslim-small.jpg" alt="">
							<a href="#tab2" class="ProductLink">Know More</a>
						</div>
				    </li>
				    <li >
						<div class="ProductCards">
							<h4>Essence</h4>
							<img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/essence-small.jpg" alt="">
							<a href="#tab3" class="ProductLink">Know More</a>
						</div>
				    </li>
			  	</ul>
			</div>


			<div id="tab1" class="tab">
				<div class="ProductFullContent">
					<div class="InsideProduct">
						<div class="container">
							<h2>Contour Elite</h2>
							<img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/contour-elite.png" alt="">
						</div>
					</div>
					<div class="featuresIcon">
						<div class="container">
							<ul>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-wireless"></use>
										</svg>
									</span>
									<p>Wireless Remote</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-headfoot"></use>
										</svg>
									</span>
									<p>Head & Foot Controls</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-flatposition"></use>
										</svg>
									</span>
									<p>One Touch Flat Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-zeroclearance"></use>
										</svg>
									</span>
									<p>Zero Clearance</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-zeroG"></use>
										</svg>
									</span>
									<p>Zero G</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-programmable"></use>
										</svg>
									</span>
									<p>2 Programmable Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-wallsaver"></use>
										</svg>
									</span>
									<p>Wall Saver</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-massage"></use>
										</svg>
									</span>
									<p>Massage</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-autohead"></use>
										</svg>
									</span>
									<p>Auto Head Tilt</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-dualusb"></use>
										</svg>
									</span>
									<p>Dual USB Ports</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-extensiondeck"></use>
										</svg>
									</span>
									<p>Extension Deck</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-zerostandby"></use>
										</svg>
									</span>
									<p>Zero Standby Power System</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-poweroutage"></use>
										</svg>
									</span>
									<p>Power Outage Protection</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-silentdrive"></use>
										</svg>
									</span>
									<p>Silent Drive Motors</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-underbed"></use>
										</svg>
									</span>
									<p>Under Bed Lighting</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-legoption"></use>
										</svg>
									</span>
									<p>Interchangeable Leg Options</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-bluetooth"></use>
										</svg>
									</span>
									<p>Bluetooth Capability</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div id="tab2" class="tab">
			    <div class="ProductFullContent">
					<div class="InsideProduct">
						<div class="container">
							<h2>Pure Slim</h2>
							<img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/pure-slim.png" alt="">
						</div>
					</div>
					<div class="featuresIcon">
						<div class="container">
							<ul>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-wireless"></use>
										</svg>
									</span>
									<p>Wireless Remote</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-headfoot"></use>
										</svg>
									</span>
									<p>Head & Foot Controls</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-flatposition"></use>
										</svg>
									</span>
									<p>One Touch Flat Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-zeroclearance"></use>
										</svg>
									</span>
									<p>Zero Clearance</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-zeroG"></use>
										</svg>
									</span>
									<p>Zero G</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-programmable"></use>
										</svg>
									</span>
									<p>2 Programmable Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-blackmetal"></use>
										</svg>
									</span>
									<p>Black Metal 6+3 Legs</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-headboard"></use>
										</svg>
									</span>
									<p>Headboard Brackets</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-outage"></use>
										</svg>
									</span>
									<p>Power Outage Protection</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-silentdrive"></use>
										</svg>
									</span>
									<p>Silent Drive Motors</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-standby"></use>
										</svg>
									</span>
									<p>Zero Standby Power System</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-bluetooth"></use>
										</svg>
									</span>
									<p>Bluetooth Capability</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div id="tab3" class="tab">
			    <div class="ProductFullContent">
					<div class="InsideProduct">
						<div class="container">
							<h2>Essence</h2>
							<img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/essence.png" alt="">
						</div>
					</div>
					<div class="featuresIcon">
						<div class="container">
							<ul>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-wireless"></use>
										</svg>
									</span>
									<p>Wireless Remote</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-headfoot"></use>
										</svg>
									</span>
									<p>Head & Foot Controls</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-flatposition"></use>
										</svg>
									</span>
									<p>One Touch Flat Position</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-zeroG"></use>
										</svg>
									</span>
									<p>Zero G</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-blackmetal"></use>
										</svg>
									</span>
									<p>Black Metal 6+3 Legs</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-headboard"></use>
										</svg>
									</span>
									<p>Headboard Brackets</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-outage"></use>
										</svg>
									</span>
									<p>Power Outage Protection</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-silentdrive"></use>
										</svg>
									</span>
									<p>Silent Drive Motors</p>
								</li>
								<li>
									<span class="iconSpan">
										<svg>
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/product.svg#icon-standby"></use>
										</svg>
									</span>
									<p>Zero Standby Power System</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>

	
	
	
</section>



</main>
<footer>
	<section class="ContactUsBlock Section">
		<div class="container">
			<h2>Contact Us</h2>
			<div class="ContactUsDetails">
				<div class="row">
					<div class="col-12 col-md-6">
						
					</div>
					<div class="col-12 col-md-6">
						<h4>Book Your Free Demo</h4>
						<div class="ContactForm MobileOnly">
							<?php echo do_shortcode( '[contact-form-7 id="5026" title="Feelgood landing Form"]' ); ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="DetailsBlock">
							<ul>
								<li>
									<span class="Icons"><img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/phone.png" alt=""></span>
									<div>
										<h3>Call Us</h3>
										<a href="tel:1800-121-189-189">1800-121-189-189</a>
									</div>
								</li>
								<li>
									<span class="Icons"><img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/email.png" alt=""></span>
									<div>
										<h3>Email Us</h3>
										<a href="mailto:feelgood@webinfo.com">feelgood@webinfo.com</a>
									</div>
								</li>
								<li>
									<span class="Icons"><img src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/img/map.png" alt=""></span>
									<div>
										<h3>Our Store</h3>
										<h5>Kirti Nagar, Delhi<br><span>4/1, Block 2, W.H.S. Kirti Nagar, New Delhi-110015</span></h5>
										<h5>M.G Road, Delhi<br><span>414, M.G Road, Ghitorni, New Delhi-110030</span></h5>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-12 col-md-6 DesktopOnly">
						<div class="ContactForm">
							<?php echo do_shortcode( '[contact-form-7 id="5026" title="Feelgood landing Form"]' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</footer>

<script src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/js/vendor.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/feelgood-landing/js/scripts.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>
