
</main>
<footer>
	<section class="ContactUsBlock Section">
		<div class="container">
			<h2>Contact Us</h2>
			<div class="ContactUsDetails">
				<div class="row">
					<div class="col-12 col-md-6">
						
					</div>
					<div class="col-12 col-md-6">
						<h4>Book Your Free Demo</h4>
						<div class="ContactForm MobileOnly">
							<form action="">
								<input type="text" placeholder="Your Name">
								<input type="email" placeholder="Email Address">
								<input type="text" placeholder="Your Subject">
								<textarea placeholder="Your Message"></textarea>
								<input type="button" value="Send Your Message">
							</form>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="DetailsBlock">
							<ul>
								<li>
									<span class="Icons"><img src="assets/img/phone.png" alt=""></span>
									<div>
										<h3>Call Us</h3>
										<a href="tel:1800-121-189-189">1800-121-189-189</a>
									</div>
								</li>
								<li>
									<span class="Icons"><img src="assets/img/email.png" alt=""></span>
									<div>
										<h3>Email Us</h3>
										<a href="mailto:feelgood@webinfo.com">feelgood@webinfo.com</a>
									</div>
								</li>
								<li>
									<span class="Icons"><img src="assets/img/map.png" alt=""></span>
									<div>
										<h3>Our Store</h3>
										<h5>Kirti Nagar, Delhi<br><span>4/1, Block 2, W.H.S. Kirti Nagar, New Delhi-110015</span></h5>
										<h5>M.G Road, Delhi<br><span>414, M.G Road, Ghitorni, New Delhi-110030</span></h5>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-12 col-md-6 DesktopOnly">
						<div class="ContactForm">
							<form action="">
								<input type="text" placeholder="Your Name">
								<input type="email" placeholder="Email Address">
								<input type="text" placeholder="Your Subject">
								<textarea placeholder="Your Message"></textarea>
								<input type="submit" value="Send Your Message">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</footer>

<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/scripts.min.js"></script>
</body>
</html>