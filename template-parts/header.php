<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>feel good</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/logo.png">
	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>

	<header class="Header">
		<div class="LogoSection">
			<div class="container">
				<a href="#"><img src="assets/img/logo.png" alt=""></a>
			</div>
		</div>

		<div class="BannerSection" style="background: url(assets/img/banner.jpg) no-repeat;">
			<div class="BannerContent">
				<div class="container">
					<h1 class="TopBar">Is It Time To <br><span>Go Adjustable?</span></h1>
				</div>
			</div>
		</div>
	</header>

	<main>